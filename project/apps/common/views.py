from random import random
from django.shortcuts import render


def home(request):
    context = {"number": random()}
    return render(request, 'home.html', context)
